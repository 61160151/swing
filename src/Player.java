
import java.io.Serializable;

public class Player implements Serializable {

    private int win, lose, draw = 0;

    private char name;

    public Player(char name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Player{" + "win=" + win + ", lose=" + lose + ", draw=" + draw + ", name=" + name + '}';
    }

    public char getName() {
        return name;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public void win() {
        this.win++;
    }

    public void lose() {
        this.lose++;
    }

    public void draw() {
        this.draw++;
    }

}
