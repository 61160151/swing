
public class Board {

    char[][] board = new char[][]{{'_', '_', '_'}, {'_', '_', '_'}, {'_', '_', '_'}};
    Player one;
    Player two;
    private Player currentPlayer;
    private Player winner;
    int Turn = 0;
    int lastRow;
    int lastCol;

    public Board(Player one, Player two) {
        this.one = one;
        this.two = two;
        this.currentPlayer = one;
    }

    public void switchPlayer() {
        if (this.currentPlayer == one) {
            this.currentPlayer = two;
        } else {
            this.currentPlayer = one;
        }
    }

    public char[][] getBoard() {
        return board;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public boolean checkWin() {
        if (checkCol() || checkRow() || checkCrosswiseL() || checkCrosswiseR()) {
            this.winner = currentPlayer;
            updateStat();
            return true;
        }
        if (Turn == 9) {
            updateStat();
            return true;
        }
        return false;
    }

    private void updateStat() {
        if (this.one == this.winner) {
            this.one.win();
            this.two.lose();

        } else if (this.two == this.winner) {
            this.two.win();
            this.one.lose();
        } else {
            this.one.draw();
            this.two.draw();
        }

    }

    public boolean checkCrosswiseR() {
        for (int i = 0; i < this.board.length; i++) {
            if (this.board[i][this.board.length - i - 1] != currentPlayer.getName()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkCrosswiseL() {
        for (int i = 0; i < this.board.length; i++) {
            if (this.board[i][i] != currentPlayer.getName()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkRow() {
        for (int col = 0; col < this.board[lastRow].length; col++) {
            if (this.board[lastRow][col] != currentPlayer.getName()) {
                return false;
            }
        }
        return true;
    }

    private boolean checkCol() {
        for (int row = 0; row < this.board.length; row++) {
            if (this.board[row][lastCol] != currentPlayer.getName()) {
                return false;
            }
        }
        return true;
    }

    public boolean setRowCol(int row, int col) {
        if (this.board[row - 1][col - 1] != '_') {
            return false;
        }
        this.board[row - 1][col - 1] = currentPlayer.getName();
        lastCol = col - 1;
        lastRow = row - 1;
        Turn++;
        return true;
    }

    public Player getWinner() {
        return winner;
    }

}
